Rails.application.routes.draw do
  resources :lesson, only: [:index]
  resources :student, only: [:show, :update]
  resources :school_class, only: [:show]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
