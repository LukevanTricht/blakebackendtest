require 'test_helper'

class StudentControllerTest < ActionDispatch::IntegrationTest
  self.use_transactional_tests = true

  test 'should update student' do
    put student_url(students(:sequence_test), format: :json), params: { student: { lesson_part_id: lesson_parts(:lesson_1_part_three).id } }
    assert_response :success
  end

  test 'should get student data' do
    get student_url(students(:student_1_class_1), format: :json)
    assert_response :success
  end

  test 'fail updating unless student provided' do
    put student_url(students(:student_1_class_1)), params: {}
    assert_response 400
  end

end
