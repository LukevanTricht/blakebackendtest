# README

## Assumptions:
* Lessons are sequential by id
* Consumers of the API can injest JSON
* Before updating a Students Progress, a list of the Lesson Parts will have been retrieved from the Lesson Controller
* SQLite is the database, but only for speed of development
* Manual Testing will be undertaken by running the application using the `test` environment



# Original Request:

PROBLEM
-------
Company X is developing a new app for student education. Students complete lessons and their progress is recorded.
Each lesson has 3 parts - 1, 2 and 3. There are 100 lessons in total.

PART 1
------
Generate an app that persists students and their progress.

Define routes for:
a) setting a student's progress - progress should consist of a lesson and part number.
b) returning a JSON representation of a student and their associated progress.

PART 2
------
Teachers have classes containing number of students.

a) Add a teacher model that is related to students
b) Create a reports page for a teacher to view the progress of all of their students.

PART 3
------
Calculating progress

a) add a method for updating student progress - this should verify that the
student is only able to complete the next part number in sequence e.g.

L1 P1, L1 P2, L1 P3, L2 P1, L2 P2 etc

PART 4 (optional)
-----------------
a) the first 50 lessons now have 5 parts. Ensure the system will still work as expected and no students progress will be lost

Please submit your application as a public git repo. Technology choices are up to you. It’d be great to see commits of how the solution came about. If you have any questions feel free to mail joshua.kunzmann@blake.com.au.
