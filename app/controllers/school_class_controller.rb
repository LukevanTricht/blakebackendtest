class SchoolClassController < ApplicationController
  def show
    school_class = SchoolClass.eager_load(students: :lesson_part).find(params[:id])

    # Render Manually to allow passing in the student
    # which allows us to avoid polluting the class namespace with variables
    render :show, locals: { school_class: school_class }
  end
end
