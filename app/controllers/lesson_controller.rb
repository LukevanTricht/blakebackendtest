class LessonController < ApplicationController

  # Returns all lesson ids, or lesson ids + part_numbers if given with_parts
  def index
    lessons = if params[:with_parts]
                Lesson.eager_load(:lesson_parts).find_each do |lesson|
                  { id: lesson.id,
                    lesson_parts: lesson.lesson_parts.pluck(:part_number) }
                end
              else
                Lesson.all.pluck(:id)
              end

    render json: lessons
  end
end
