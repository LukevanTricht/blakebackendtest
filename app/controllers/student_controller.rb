class StudentController < ApplicationController

  def show
    student = Student.eager_load(:lesson_part).find(params[:id])
    # Render Manually to allow passing in the student
    # which allows us to avoid polluting the class namespace with variables
    render :show, locals: { student: student }
  end

  def update
    student = Student.find(params[:id])

    return head 400 unless student.update!(safe_update_params)

    # Render Manually to allow passing in the student
    # which allows us to avoid polluting the class namespace with variables
    render :show, locals: { student: student }
  rescue ActionController::ParameterMissing
    head 400
  end

  private

  def safe_update_params
    params.require(:student).permit(:name, :lesson_part_id)
  end
end
