json.extract! school_class, :id

json.students do
  json.array! school_class.students do |student|
    json.name student.name
    json.progress do
      json.lesson student.lesson_part.lesson_id
      json.part_number student.lesson_part.part_number
    end
  end
end
