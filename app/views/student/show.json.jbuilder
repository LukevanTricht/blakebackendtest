json.extract! student, :name

json.progress do
  json.extract! student.lesson_part, :lesson_id, :part_number
end
