class Student < ApplicationRecord
  belongs_to :school_class
  belongs_to :lesson_part

  validate :lesson_part_changed_and_valid, on: :update

  def lesson_part_changed_and_valid
    if changed.include?('lesson_part_id')
      errors.add('lesson_part', 'Lesson Parts need to be sequential') unless LessonPart.find(lesson_part_id_was).is_next_in_sequence?(lesson_part)
    end
  end
end
