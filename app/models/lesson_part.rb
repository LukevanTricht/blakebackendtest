class LessonPart < ApplicationRecord
  belongs_to :lesson
  validates :part_number, uniqueness: { scope: :lesson, message: 'Parts should be unique per lesson' }

  # Determine if the new_part passed in is next in sequence of lessons and parts
  def is_next_in_sequence? new_part

    # If they're in the same lesson, and the part_number is sequential
    return true if lesson_id == new_part.lesson_id && (part_number + 1) == new_part.part_number

    # If there's no lesson part after this one in the same lesson, the new parts lesson is next in sequence, and the new part is the first part of the new lesson
    return true if LessonPart.exists?(lesson: lesson, part_number: part_number + 1) == false && (lesson_id + 1) == new_part.lesson_id && new_part.part_number == 1

    # If it's not the next lesson part, and not part of the next lesson, it's not
    false
  end

end
