class CreateLessonParts < ActiveRecord::Migration[5.2]
  def change
    create_table :lesson_parts do |t|
      t.references :lesson, foreign_key: true
      t.integer :part_number

      t.timestamps
    end

    add_index :lesson_parts, %i[part_number lesson_id], unique: true
  end
end
