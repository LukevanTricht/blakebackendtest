class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :name
      t.references :lesson_part
      t.references :school_class

      t.timestamps
    end
  end
end
